package com.atttttaboy.firebaselearning;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{

  SignInButton btnLoginGoogle;

  private static final int RC_SIGN_IN = 9001;
  Integer pass = 0;

  private EditText etEmail, etPassword;
  private Button btnSignout;

  GoogleApiClient mGoogleApiClient;

  private FirebaseAuth fAuth;
  private FirebaseAuth.AuthStateListener fStateListener;

  private static final String TAG = MainActivity.class.getSimpleName();
  String namaku;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
        .requestEmail()
        .build();
    mGoogleApiClient = new GoogleApiClient.Builder(this)
        .enableAutoManage(this, this)
        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
        .build();

    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    getSupportActionBar().setTitle("Belajar Firebase");

    fAuth = FirebaseAuth.getInstance();

    fStateListener = new FirebaseAuth.AuthStateListener() {
      @Override
      public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null){
          Log.d(TAG, "onAuthStateChanged:signed_in:"+ user.getUid());
        } else {
//          Toast.makeText(MainActivity.this, "User Logout", Toast.LENGTH_SHORT).show();
          btnSignout.setVisibility(View.GONE);
          Log.v(TAG, "onAuthStateChanged:signed_out");
        }
      }
    };

    Button btnSignup = (Button) findViewById(R.id.btn_daftar);
    Button btnLogin = (Button) findViewById(R.id.btn_login);
    btnLoginGoogle = (SignInButton) findViewById(R.id.btn_googlelogin);
    btnSignout = (Button) findViewById(R.id.btn_logout);
    etEmail = (EditText) findViewById(R.id.et_email);
    etPassword = (EditText) findViewById(R.id.et_password);

    btnLogin.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        pass = 1;
        login(etEmail.getText().toString(), etPassword.getText().toString());
      }
    });

    btnLoginGoogle.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        pass = 2;
        googleSignin();
      }
    });

    btnSignup.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        daftar(etEmail.getText().toString(), etPassword.getText().toString());
      }
    });

    btnSignout.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        logout();
      }
    });

  }

  private void googleSignin() {
    Intent googleSignin = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
    startActivityForResult(googleSignin, RC_SIGN_IN);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (requestCode == RC_SIGN_IN){
      GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
      handleSignInResult(result);
    }
  }

  private void handleSignInResult(GoogleSignInResult result) {
    Log.d(TAG, "handleSignInResult:" + result.isSuccess());
    if (result.isSuccess()){
      btnSignout.setVisibility(View.VISIBLE);
      GoogleSignInAccount acc = result.getSignInAccount();
      namaku = acc.getDisplayName();
      Toast.makeText(MainActivity.this, "Hallo " + acc.getDisplayName(), Toast.LENGTH_SHORT).show();
    } else {

    }
  }


  private void login(final String email, String password) {
    fAuth.signInWithEmailAndPassword(email, password)
        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
          @Override
          public void onComplete(@NonNull Task<AuthResult> task) {
           Log.d(TAG, "signInWithEmail:onComplete" + task.isSuccessful());

            if (!task.isSuccessful()){
              Log.w(TAG, "signInWithEmail:failed", task.getException());
              Toast.makeText(MainActivity.this, "Proses Login Gagal", Toast.LENGTH_SHORT).show();
            } else {
              btnSignout.setVisibility(View.VISIBLE);
              Toast.makeText(MainActivity.this, "Proses Login Berhasil\n"+
                  "Email : " + email, Toast.LENGTH_SHORT).show();
            }
          }
        });
  }

  private void daftar(final String email, String password) {
    fAuth.createUserWithEmailAndPassword(email, password)
        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
          @Override
          public void onComplete(@NonNull Task<AuthResult> task) {
            Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

            if (!task.isSuccessful()){
              task.getException().printStackTrace();
              Toast.makeText(MainActivity.this, "Proses Pendaftaran Gagal", Toast.LENGTH_SHORT).show();
            } else {
              Toast.makeText(MainActivity.this, "Proses Pendaftaran Berhasil\n"+
                  "Email : " + email, Toast.LENGTH_SHORT).show();
            }
          }
        });
  }

  private void logout(){
    if (pass ==  1){
      Toast.makeText(MainActivity.this, "Berhasil Logout", Toast.LENGTH_SHORT).show();
      fAuth.signOut();
    } else if (pass == 2){
      Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
          new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
              btnSignout.setVisibility(View.GONE);
              Toast.makeText(MainActivity.this, "Dadah " + namaku, Toast.LENGTH_SHORT).show();
            }
          });
    }
  }

  @Override
  protected void onStart() {
    super.onStart();
    fAuth.addAuthStateListener(fStateListener);
  }

  @Override
  protected void onStop() {
    super.onStop();
    if (fStateListener != null){
      fAuth.removeAuthStateListener(fStateListener);
    }
  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

  }
}
